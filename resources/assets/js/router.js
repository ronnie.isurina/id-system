import Vue from 'vue'
import VueRouter from 'vue-router'


import Login from './components/other/Login.vue'
import Layout from './theme/Layout.vue'

/* Layout Child */
import Dashboard from './components/dashboard/Dashboard.vue'
import NotFound from './components/error/NotFound.vue'
import Soon from './components/error/Soon.vue'

/* User */
import User from './components/user/User.vue'
import UserProfile from './components/user/Profile.vue'

/* HR */
import Employee from           './components/hr/employee/Index.vue'
import EmployeeProfile from './components/hr/employee/Profile.vue'
import Worker from           './components/hr/worker/Index.vue'
import Agent from           './components/hr/agent/Index.vue'

/* List Of Account */
import Brand from           './components/list_of_account/brand/Index.vue'
import ChartOfAccount from  './components/list_of_account/chart_of_account/Index.vue'
import Equipment from       './components/list_of_account/equiptment/Index.vue'
import EquipmentProfile from './components/list_of_account/equiptment/Profile.vue'
import Material from        './components/list_of_account/material/Index.vue'
import MaterialProfile from './components/list_of_account/material/Profile.vue'
import MaterialType from    './components/list_of_account/material_type/Index.vue'
import UnitOfMeasure from   './components/list_of_account/unit_of_measure/Index.vue'
import OtherExpense from    './components/list_of_account/other_expense/Index.vue'
import Supplier from        './components/list_of_account/supplier/Index.vue'
import Supply from          './components/list_of_account/supply/Index.vue'
import Tool from            './components/list_of_account/tool/Index.vue'
import ToolProfile from './components/list_of_account/tool/Profile.vue'
import Utility from         './components/list_of_account/utility/Index.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  linkActiveClass: '',
  scrollBehavior: (to, from, savedPosition) => ({ y: 0 }),
  routes: [
    { path: '/login', component: Login },
    { path: '',
      component: Layout,
      children: [
        //Dashboard
        { path: '/', name: 'dashboard', component: Dashboard },
        
        //Projects
        { path: '/material',  name: 'material',  component: Material },
        { path: '/material/:id/profile', name: 'material_profile', component: MaterialProfile },
        { path: '/material-type',  name: 'material-type',  component: MaterialType },
        { path: '/unit-of-measure',  name: 'unit-of-measure',  component: UnitOfMeasure },
        { path: '/brand',  name: 'brand',  component: Brand },
        { path: '/equipment',  name: 'equipment',  component: Equipment },
        { path: '/equipment/:id/profile', name: 'equipment_profile', component: EquipmentProfile },
        { path: '/tool',  name: 'tool',  component: Tool },
        { path: '/tool/:id/profile', name: 'tool_profile', component: ToolProfile },
        { path: '/utility',  name: 'utility',  component: Utility },
        { path: '/supply',  name: 'supply',  component: Supply },
        { path: '/other-expense',  name: 'other-expense',  component: OtherExpense },
        { path: '/supplier',  name: 'supplier',  component: Supplier },
        
        //HR
        { path: '/payroll',  name: 'payroll',  component: Soon },
        { path: '/employee',  name: 'employee',  component: Employee },
        { path: '/employee/:id/profile', name: 'employee_profile', component: EmployeeProfile },
        { path: '/worker',  name: 'worker',  component: Worker },
        { path: '/agent',  name: 'agent',  component: Agent },

        //Accounting
        { path: '/chart-of-account', name: 'chart-of-account', component: ChartOfAccount },

        //Investor
        { path: '/investor', name: 'investor', component: Soon },

        //Setting
        { path: '/user', name: 'user', component: User },
        { path: '/user/:id/profile', name: 'user_profile', component: UserProfile },

        //Other
        { path: '*', component: NotFound }
      ]
    },
  ] 
})

export default router
