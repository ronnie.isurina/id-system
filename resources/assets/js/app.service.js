import axios from 'axios'
import router from './router.js'

axios.defaults.baseURL = window.api_url

axios.interceptors.request.use(function (config) {
  const token = window.localStorage.getItem('token')
  if (token) {
    config.headers.Authorization = `Bearer ${token}`
  }

  return config
})

const crud = {
  generate_url(query) {
    query.page = (query.page != '' || query.page != 'page=1') ? query.page : ''
    query.search_key = (query.search_key) ? '&search_key=' + query.search_key : ''
    query.show_row = (query.show_row != 10) ? '&per_page=' + query.show_row : ''
    query.limit = (query.limit != 100) ? '&limit=' + query.limit : ''
    
    let url = query.base + '?'
    query.base = ''
    for (let [key, value] of Object.entries(query)) {
      url += (key != 0) ? value : '&' + value
    }
    return url
  },
  gets (url) {
    return new Promise((resolve) => {
      axios.get(url).then(response => {
        resolve(response.data)
      })
    })
  },
  store (url, credentials) {
    return new Promise((resolve, reject) => {
      axios.post(url,  credentials ).then(response => {
        resolve(response.data)
      })
    })
  },
  update (url, credentials) {
    return new Promise((resolve, reject) => {
      axios.put( url, credentials ).then(response => {
        resolve(response.data)
      })
    })
  },
  delete(url) {
    return new Promise((resolve, reject) => {
      axios.delete(url).then(response => {
        resolve(response.data)
      })
    })
  }
}

const appService = {
  login (credentials) {
    return new Promise((resolve, reject) => {
      axios.post('/auth/login', credentials)
        .then(response => {
          resolve(response.data)
        }).catch(response => {
          reject(response.status)
        })
    })
  },
  crud
}

export default appService
