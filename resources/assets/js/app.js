require('./bootstrap');

import Vue from 'vue'
import router from './router'   
import store from './vuex/index.js'
import MainLayout from './theme/Main.vue'
import numeral from 'numeral'
  
const app = new Vue({
    el: '#app',
    router,
    components: {
      layout : MainLayout
    },
    store
})

Vue.filter("formatNumber", function(value) {
  return numeral(value).format("0,0.00")
})

export { app, router , store}
