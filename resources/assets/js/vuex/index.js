import Vue from 'vue'
import router from '../router.js'
import Vuex from 'vuex'
import appService from '../app.service.js'
import optionListModule from './optionList.js'

Vue.use(Vuex)

const state = {
    app_name: window.app_name,
    isAuthenticated: false,
    user_details: '',
}

const store = new Vuex.Store({
    modules: {
        optionListModule
    },
    state,
    getters: {
        isAuthenticated: (state) => {
            return state.isAuthenticated
        },
        user_details: (state) => {
            return state.user_details
        }
    },
    actions: {
        logout (context) {
            return new Promise((resolve, reject) => {
                let token = window.localStorage.getItem('token')
                let url =  `/auth/logout?token=${token}`
                axios.post(url).then(() => {
                    context.commit('logout')
                    router.push('/login')
                })
            })
        },
        login (context, credentials) {
            return new Promise((resolve) => {
                appService.login(credentials)
                .then((data) => {
                    context.commit('login', data)
                    if (!data.error) {
                        router.go(-1)
                    }else{ 
                        resolve ( data )
                    }
                }).catch(( reponse ) => {
                    context.commit('login', {
                        error: 'Can\'t connect to server!'
                    })

                    if (typeof window !== 'undefined') { 
                        window.console.log({
                            error: 'Can\'t connect to server!'
                        })
                    }
                })
            })
        },
        authentication (context) {
            context.commit('authentication')
        }              
    },
    mutations: {
        logout (state) {
            if (typeof window !== 'undefined') {
                window.localStorage.setItem('token', null)
                window.localStorage.setItem('tokenExpiration', null)
            }
            state.isAuthenticated = false
            state.user_details = ''
        }, 
        login (state, token) {
            state.isAuthenticated = true
            if (typeof window !== 'undefined') {
                window.localStorage.setItem('token', token.access_token)
                window.localStorage.setItem('tokenExpiration', token.expires_in)
            }
        },
        authentication (state) {
            const token = window.localStorage.getItem('token')
            const expiration = window.localStorage.getItem('tokenExpiration')

            if (typeof window !== 'undefined') {
                var unixTimestamp = new Date().getTime() / 1000

                if (expiration !== null && parseInt(expiration) - unixTimestamp > 0 && token != null && token != '') {            
                    let url =  `/auth/me`
                    axios.post( url ).then(response => {
                        state.user_details = response.data
                        state.isAuthenticated = true

                        if( router.currentRoute.fullPath == '/login') {
                            router.push('/')
                        }
                    }).catch(response => {
                        router.push('/login')
                    })
                } else {
                    state.isAuthenticated = false
                    router.push('/login')
                }
            }
        }
    }
})

export default store
