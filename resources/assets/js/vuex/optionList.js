import appService from '../app.service.js'

const state = {
    brands: [],
    material_types: [],
    unit_of_measures: [],
    last_update: '',
  }

const getters = {
    brands: state => state.brands,
    material_types: state => state.material_types,
    unit_of_measures: state => state.unit_of_measures,
}

const actions = {
  update_options (context) {
    /* Brand */
    appService.crud.gets('brand').then(data => {
        context.commit('update_brand', data.brands.data)
    })

    /* Material Type */
    appService.crud.gets('material-type').then(data => {
        context.commit('update_material_type', data.material_types.data)
    })

      /* Unit Of Measure */
    appService.crud.gets('unit-of-measure').then(data => {
        context.commit('update_unit_of_measure', data.unit_of_measures.data)
    })
  }
}

const mutations = {
    update_brand (state, brands) {
        state.brands = brands
    },

    update_material_type (state, material_types) {
        state.material_types = material_types
    },

    update_unit_of_measure (state, unit_of_measures) {
        state.unit_of_measures = unit_of_measures
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
