
window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');

    /**
	* Inpinia Theme
    **/
    // Mainly scripts
    require('bootstrap')
    require('metismenu')
    require('jquery-slimscroll')

    // Flot
    require('../inspinia/js/plugins/flot/jquery.flot.js')
    require('../inspinia/js/plugins/flot/jquery.flot.tooltip.min.js')
    require('../inspinia/js/plugins/flot/jquery.flot.spline.js')
    require('../inspinia/js/plugins/flot/jquery.flot.resize.js')
    require('../inspinia/js/plugins/flot/jquery.flot.pie.js')
    require('../inspinia/js/plugins/flot/jquery.flot.symbol.js')
    require('../inspinia/js/plugins/flot/jquery.flot.time.js')

    // Custom and plugin javascript
    require('../inspinia/js/inspinia.js')
    require('../inspinia/js/plugins/pace/pace.min.js')

    //Chosen
    require('../inspinia/js/plugins/chosen/chosen.jquery.js')

    //jQuery UI
    require('../inspinia/js/plugins/jquery-ui/jquery-ui.min.js')

    //Jvectormap
    require('~/../../node_modules/jvectormap/jquery-jvectormap.min.js')
    require('../inspinia/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')

    //EayPIE
    require('~/../../node_modules/easy-pie-chart/dist/jquery.easypiechart.min.js')

    //Sparkline
    require('../inspinia/js/plugins/sparkline/jquery.sparkline.min.js')

    //Sparkline demo data 
    require('../inspinia/js/demo/sparkline-demo.js')

    //iCheck
    require('../inspinia/js/plugins/iCheck/icheck.min.js')

    /** Toastr **/
    window.toastr = require('toastr')
    toastr.options = {
        closeButton: true,
        progressBar: true,
        showMethod: 'slideDown',
        timeOut: 10000
    }
    /** End Toastr **/

    /** Choosen Defaulr **/
    $('.chosen-select').chosen({width: "100%"})
    /** End  Choosen **/

} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key'
// });
// 

/*
 * Romanize Convert Number to Roman Numeral
*/

window.romanize = require('romanize')