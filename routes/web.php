<?php

Route::group( [ 'prefix' => '/' ], function () {
    Route::get('', function () {
        return view('app');
    });

    Route::any( '/{any?}',  function ($any) {
        return view('app');
    })->where('any', '.*');
});
