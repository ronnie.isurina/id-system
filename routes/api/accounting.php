<?php

Route::group([ 'prefix' => 'v1',  'middleware' => 'auth:api' ], function() {

    /** General Account **/
    Route::resource( 
        'general-account', 
        'GeneralAccountController', 
        [
            'except' => [ 'edit', 'create' ],
            'parameters' => [ 'general-account' => 'id' ],
        ]
    );

    /** Chart Of Account **/
    Route::resource( 
        'chart-of-account', 
        'ChartOfAccountController', 
        [
            'except' => [ 'edit', 'create' ],
            'parameters' => [ 'chart-of-account' => 'id' ],
        ]
    );

    /** Company Account **/
    Route::resource( 
        'company-account', 
        'CompanyAccountController', 
        [
            'except' => [ 'edit', 'create' ],
            'parameters' => [ 'company-account' => 'id' ],
        ]
    );

    /** Company Account **/
    Route::resource( 
        'company-sub-account', 
        'CompanySubAccountController', 
        [
            'except' => [ 'edit', 'create' ],
            'parameters' => [ 'company-sub-account' => 'id' ],
        ]
    );
});