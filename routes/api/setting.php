<?php

Route::group([ 'prefix' => 'v1',  'middleware' => 'auth:api' ], function() {

    /** User **/
    Route::resource( 
        'user',  
        'UserController', 
        [ 'except' => [ 'edit', 'create' ],
        'parameters' => [ 'user' => 'id' ],
    ]);
    
    Route::group([ 'prefix' => 'user' ], function() {
        /* Reset User Password */
        Route::put('/{id}/passwordreset', [
            'uses' => 'UserController@password_reset',
            'as' => 'user.password_reset'
        ]);

        /* Add User role */
        Route::post('/{user_id}/role', [
            'uses' => 'UserController@store_role',
            'as' => 'user.store_role'
        ]);

        Route::delete('/{user_id}/role', [
            'uses' => 'UserController@destroy_role',
            'as' => 'user.destroy_role'
        ]);
    });
});