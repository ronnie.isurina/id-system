<?php

Route::group([ 'prefix' => 'v1',  'middleware' => 'auth:api' ], function() {

    /** Unit Of Measure **/
    Route::resource( 
        'unit-of-measure', 
        'UnitOfMeasureController', 
        [
            'except' => [ 'edit', 'create' ],
            'parameters' => [ 'unit-of-measure' => 'id' ],
        ]
    );

    /** Material Type **/
    Route::resource( 
        'material-type', 
        'MaterialTypeController', 
        [
            'except' => [ 'edit', 'create' ],
            'parameters' => [ 'material-type' => 'id' ],
        ]
    );

    /** Material **/
    Route::resource( 
        'material', 
        'MaterialController', 
        [
            'except' => [ 'edit', 'create' ],
            'parameters' => [ 'material' => 'id' ],
        ]
    );
    Route::group([ 'prefix' => 'material' ], function() {
        /* Additional Material Brand */
        Route::post('/{id}', [
            'uses' => 'MaterialController@store_brand',
            'as' => 'material.store_brand'
        ]);
    });

    /** Tools **/
    Route::resource( 
        'tool', 
        'ToolController', 
        [
            'except' => [ 'edit', 'create' ],
            'parameters' => [ 'tool' => 'id' ],
        ]
    );
    Route::group([ 'prefix' => 'tool' ], function() {
        /* Additional Tool Brand */
        Route::post('/{id}', [
            'uses' => 'ToolController@store_brand',
            'as' => 'tool.store_brand'
        ]);
    });

    /** Equipment **/
    Route::resource( 
        'equipment', 
        'EquipmentController', 
        [
            'except' => [ 'edit', 'create' ],
            'parameters' => [ 'equipment' => 'id' ],
        ]
    );
    Route::group([ 'prefix' => 'equipment' ], function() {
        /* Additional Equipment Brand */
        Route::post('/{id}', [
            'uses' => 'EquipmentController@store_brand',
            'as' => 'equipment.store_brand'
        ]);
    });

     /** Brand **/
     Route::resource( 
        'brand', 
        'BrandController', 
        [
            'except' => [ 'edit', 'create' ],
            'parameters' => [ 'brand' => 'id' ],
        ]
    );

    /** Item **/
    Route::group([ 'prefix' => 'item' ], function() {
        /* Additional Brand */
        Route::post('{type}/{id}', [
            'uses' => 'ItemController@store',
            'as' => 'item.store'
        ]);

        Route::delete('{id}', [
            'uses' => 'ItemController@destroy',
            'as' => 'item.destroy'
        ]);
    });

     /** OtherExpense **/
     Route::resource( 
        'other-expense', 
        'OtherExpenseController', 
        [
            'except' => [ 'edit', 'create' ],
            'parameters' => [ 'other-expense' => 'id' ],
        ]
    );

    /** Supplier **/
    Route::resource( 
        'supplier', 
        'SupplierController', 
        [
            'except' => [ 'edit', 'create' ],
            'parameters' => [ 'supplier' => 'id' ],
        ]
    );

    /** Supply **/
    Route::resource( 
        'supply', 
        'SupplyController', 
        [
            'except' => [ 'edit', 'create' ],
            'parameters' => [ 'supply' => 'id' ],
        ]
    );

    /** Utility **/
    Route::resource( 
        'utility', 
        'UtilityController', 
        [
            'except' => [ 'edit', 'create' ],
            'parameters' => [ 'utility' => 'id' ],
        ]
    );
});