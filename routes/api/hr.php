<?php

Route::group([ 'prefix' => 'v1',  'middleware' => 'auth:api' ], function() {
    
    # Employee
    Route::group(['prefix' => 'employee'], function () {
        Route::resource( 
            '', 
            'HR\EmployeeController', 
            [
                'except' => [ 'edit', 'create' ],
                'parameters' => [ '' => 'id' ],
            ]
        );

        Route::resource( 
            '{employee_id}/pan', 
            'HR\EmployeePanController', 
            [
                'except' => [ 'index', 'edit', 'create' ],
                'parameters' => [ 'pan' => 'id' ],
            ]
        );
        
        /* Saved PAN */
        Route::put('/{employee_id}/pan/{pan_id}/save', [
            'uses' => 'HR\EmployeePanController@save',
            'as' => 'pan.save'
        ]);
    });

    # Worker
    Route::resource( 
        'worker', 
        'HR\WorkerController', 
        [
            'except' => [ 'edit', 'create' ],
            'parameters' => [ 'worker' => 'id' ],
        ]
    );

    # Agent
    Route::resource( 
        'agent', 
        'HR\AgentController', 
        [
            'except' => [ 'edit', 'create' ],
            'parameters' => [ 'agent' => 'id' ],
        ]
    );
});