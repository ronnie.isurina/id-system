<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * Get all of the owning items models.
     */
    public function itemTable()
    {
        return $this->morphTo();
    }

    public function items() {
        return $this->hasMany('App\Item');
    }

    public function materials()
    {
        return $this->morphedByMany('App\Material', 'items');
    }

    public function tools()
    {
        return $this->morphedByMany('App\Tool', 'items');
    }

    public function equipments()
    {
        return $this->morphedByMany('App\Equipment', 'items');
    }
}
