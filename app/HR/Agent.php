<?php

namespace App\HR;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agent extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at' ];

    protected $appends = [ 'full_name', 'display_started_date' ];

    protected $fillable = [
        'last_name',
        'first_name',
        'middle_name',
        'started_date',
        'contact_num',
        'email',
        'presented_identification',
        'presented_identification_num',
    ];

    public function getFullNameAttribute() {
        $last_name = $this->attributes['last_name'];
        $first_name = $this->attributes['first_name'];
        $middle_name = $this->attributes['middle_name'];

        return generateFullName($last_name, $first_name, $middle_name);
    }

    public function getDisplayStartedDateAttribute(){
        return fullDate( $this->attributes['started_date'] );
    }
}
