<?php

namespace App\HR;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at' ];

    protected $appends = [ 
        'full_name', 
        'display_hired_date',  
        'display_birth_date',
        'latest_pan'
    ];

    protected $fillable = [
        'last_name',
        'first_name',
        'middle_name',
        'hired_date',
        'birth_date',
        'civil_status',
        'address',
        'contact_num',
        'spouse_name',
        'no_of_children',
        'no_of_dependent',
        'mother_name',
        'sss_no',
        'tin_no',
        'philhealth_no',
        'hdmf_no',
        'hdmf_ee',
        'payroll_account',
    ];

    public function employee_pans() {
        return $this->hasMany('App\HR\EmployeePan')->orderBy('id', 'desc');
    }

    public function getFullNameAttribute() {
        $last_name = $this->attributes['last_name'];
        $first_name = $this->attributes['first_name'];
        $middle_name = $this->attributes['middle_name'];

        return generateFullName($last_name, $first_name, $middle_name);
    }

    public function getDisplayHiredDateAttribute(){
        return fullDate( $this->attributes['hired_date'] );
    }
    
    public function getDisplayBirthDateAttribute(){
        return fullDate( $this->attributes['birth_date'] );
    }

    public function getlatestPanAttribute(){
        return $this->employee_pans()->where('is_saved', true)->orderBy('id', 'desc')->first();
    }
}
