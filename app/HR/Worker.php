<?php

namespace App\HR;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Worker extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at' ];

    protected $appends = [ 'full_name', 'display_hired_date' ];

    protected $fillable = [
        'last_name',
        'first_name',
        'middle_name',
        'hired_date',
    ];

    public function getFullNameAttribute() {
        $last_name = $this->attributes['last_name'];
        $first_name = $this->attributes['first_name'];
        $middle_name = $this->attributes['middle_name'];

        return generateFullName($last_name, $first_name, $middle_name);
    }

    public function getDisplayHiredDateAttribute(){
        return fullDate( $this->attributes['hired_date'] );
    }
}
