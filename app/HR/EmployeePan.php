<?php

namespace App\HR;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeePan extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $appends = [
        'total_allowance',
        'total_salary',
        'display_effectivity',
        'superior_name'
    ];

    protected $fillable = [
        'employee_id',
        'nature',
        'department',
        'job_title',
        'superior',
        'pay_class',
        'effectivity',
        'basic_salary',
        'other_allow',
        'rice_allow',
        'laundry_allow',
        'clothing_allow',
        'medical_allow',
        'ob_allow',
        'ecola',
        'pay_13th',
        'remarks',
    ];

    public function employee() {
        return $this->belongsTo('App\HR\Employee');
    }

    public function superior() {
        return $this->hasOne('App\HR\Employee', 'id', 'superior');
    }

    public function getTotalAllowanceAttribute() {
        $total = 0;
        $total += $this->attributes['rice_allow'];
        $total += $this->attributes['laundry_allow'];
        $total += $this->attributes['clothing_allow'];
        $total += $this->attributes['medical_allow'];
        $total += $this->attributes['ob_allow'];
        $total += $this->attributes['ecola'];
        return $total;
    }

    public function getTotalSalaryAttribute() {
        return $this->getTotalAllowanceAttribute() + $this->attributes['basic_salary'];
    }

    public function getDisplayEffectivityAttribute(){
        return fullDate( $this->attributes['effectivity'] );
    }

    public function getSuperiorNameAttribute(){
        $superior = $this->superior()->first(); 
        return ($superior) ? $superior->full_name : '';
    }
}
