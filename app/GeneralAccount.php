<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GeneralAccount extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * Get all of the chart of accounts item.
    */
    public function chart_of_accounts()
    {
        return $this->hasMany('App\ChartOfAccount');
    }
}
