<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['brand_id'];

    public function brand()
    {
        return $this->belongsTo( 'App\Brand' );
    }

    /**
     * Get all of the owning items models.
     */
    public function itemtable()
    {
        return $this->morphTo();
    }
}
