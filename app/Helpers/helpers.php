<?php
use Carbon\Carbon;

if (!function_exists('fullDate')) {

    function fullDate( $date, $format = 'F d, Y' )
    {
        return Carbon::parse($date)->format($format);
    }
}

if( !function_exists('moneyFormat') ) {
    function moneyFormat( $amount ) {
        if ($amount) {
            return '0.00';
        }
        return number_format($amount, 2, '.', ',');
    }
}

if (!function_exists('generateFullName')) {
    function generateFullName($last_name, $first_name, $middle_name) {
        $middle_name = explode(' ', $middle_name);

        $mid_name = '';
        foreach ($middle_name as $middle) {
            $mid_name .= $middle[0];
        }
        $middle_name = ($mid_name != '') ? $mid_name . '.' : '';
        
        $full_name = $last_name . ', ' . $first_name  . ' ' . $middle_name;
        return ucwords($full_name);
    }
}