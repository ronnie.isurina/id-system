<?php

namespace App\Http\Controllers;

use App\ChartOfAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ChartOfAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = ($request->query('per_page')) ? $request->query('per_page') : 10 ;
        $limit = ($request->query('limit')) ? $request->query('limit') : 100 ;
        $search_key = '%'.$request->query('search_key').'%';

        $chart_of_accounts = ChartOfAccount::where('code', 'like', $search_key)
                        ->paginate($per_page);

        $chart_of_accounts->withPath('');

        $response = [
            'message' => 'Chart of Account',
            'chart_of_accounts' => $chart_of_accounts
        ];
        return response()->json( $response, 200 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255|unique:brands',
            'description' => 'required|string|unique:brands',
            'general_account' => 'required|integer',
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }

        $chart_of_account = new ChartOfAccount;
        $chart_of_account->code = $request->input('code');
        $chart_of_account->description = $request->input('description');
        $chart_of_account->general_account_id = $request->input('general_account');

        $chart_of_account->save();

        $response = [
            'message' =>  $chart_of_account->code . ' added to Brand!',
            'chart_of_account' => $chart_of_account,
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [
            'chart_of_account' => ChartOfAccount::find($id)
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255',
            'description' => 'required|string',
            'general_account' => 'required|integer',
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }
  
        $chart_of_account = ChartOfAccount::find( $id );
        $chart_of_account->code = $request->input('code');
        $chart_of_account->description = $request->input('description');
        $chart_of_account->general_account_id = $request->input('general_account');

        // Additional Validate If code is exist
        $chart_of_account_code_check = ChartOfAccount::where('code', $name)->where('id', '!=', $id);
        if( $chart_of_account_code_check->count() ){
            return response()->json( [ 'errors' => [
                'code' => ['The code has already used.']
            ] ], 200 );
        }

        $chart_of_account_description_check = ChartOfAccount::where('description', $name)->where('id', '!=', $id);
        if( $chart_of_account_code_check->count() ){
            return response()->json( [ 'errors' => [
                'description' => ['The description has already used.']
            ] ], 200 );
        }

        $chart_of_account->save();

        $response = [
            'message' =>  $chart_of_account->code . ' was updated!',
            'chart_of_account' => $chart_of_account
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Delete the specified resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */

    public function destroy($id) {
        $chart_of_account = ChartOfAccount::find( $id );

        $chart_of_account->delete();

        $response = [
            'message' => $chart_of_account->unit . 'was deleted!',
            'chart_of_account' => $chart_of_account
        ];

        return response()->json( $response, 200 );

    }
}
