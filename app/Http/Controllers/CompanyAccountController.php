<?php

namespace App\Http\Controllers;

use App\CompanyAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CompanyAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = ($request->query('per_page')) ? $request->query('per_page') : 10 ;
        $limit = ($request->query('limit')) ? $request->query('limit') : 100 ;
        $search_key = '%'.$request->query('search_key').'%';

        $company_accounts = CompanyAccount::where('code', 'like', $search_key)
                        ->orderBy('id', 'desc');

        $company_accounts = ($request->query('for_list')) 
                        ? $company_accounts->get()
                        : $company_accounts->paginate($per_page,['*'],'sub_page')
                                            ->withPath('');

        $response = [
            'message' => 'List of Company Accounts',
            'company_accounts' => $company_accounts
        ];
        return response()->json( $response, 200 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255|unique:company_accounts',
            'description' => 'required|string|unique:company_accounts',
        ]);
  
        if ($validator->fails()) {
            /* Check if Trash */
            $trash_company_account = CompanyAccount::withTrashed()->where('code', $request->input('code') )->first();
            if( $trash_company_account ) {
                if( $trash_company_account->trashed() ) {
                    $trash_company_account->restore();
                    return response()->json( [
                        'message' =>  $trash_company_account->code . ' was restored!',
                        'company_account' => $trash_company_account,
                    ], 200 );
                }
            }
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }

        $company_account = new CompanyAccount;
        $company_account->code = $request->input('code');
        $company_account->description = $request->input('description');

        $company_account->save();

        $response = [
            'message' =>  $company_account->code . ' added to Company Account!',
            'company_account' => $company_account,
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [
            'company_account' => CompanyAccount::find($id)
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255',
            'description' => 'required|string',
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }
        $code = $request->input('code');

        $company_account = CompanyAccount::find( $id );
        $company_account->code = $code;
        $company_account->description = $request->input('description');

        // Additional Validate If code is exist
        $company_account_code_check = CompanyAccount::where('code', $code)->where('id', '!=', $id);
        if( $company_account_code_check->count() ){
            return response()->json( [ 'errors' => [
            'code' => ['The code has already used.']
            ] ], 200 );
        }

        $company_account_description_check = CompanyAccount::where('description', $code)->where('id', '!=', $id);
        if( $company_account_code_check->count() ){
            return response()->json( [ 'errors' => [
            'description' => ['The description has already used.']
            ] ], 200 );
        }

        $company_account->save();

        $response = [
            'message' =>  $company_account->code . ' was updated!',
            'company_account' => $company_account
        ];

        return response()->json( $response, 200 );
    }

     /**
     * Delete the specified resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */

    public function destroy($id) {
        $company_account = CompanyAccount::find( $id );

        if($company_account->company_sub_accounts->count()) {
            return response()->json( [
                'message' => $company_account->code . ' can\'t be delete!',
            ] );
        }

        $company_account->delete();

        $response = [
            'message' => $company_account->code . ' was deleted!',
            'errors' => false,
            'company_account' => $company_account
        ];

        return response()->json( $response, 200 );

    }
}
