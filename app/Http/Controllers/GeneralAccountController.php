<?php

namespace App\Http\Controllers;

use App\GeneralAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class GeneralAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = ($request->query('per_page')) ? $request->query('per_page') : 10 ;
        $limit = ($request->query('limit')) ? $request->query('limit') : 100 ;
        $search_key = '%'.$request->query('search_key').'%';

        $general_accounts = GeneralAccount::where('code', 'like', $search_key)
                        ->paginate($per_page);

        $general_accounts->withPath('');

        $response = [
            'message' => 'General Account list',
            'general_accounts' => $general_accounts
        ];
        return response()->json( $response, 200 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255|unique:brands',
            'description' => 'required|string|unique:brands',
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }

        $general_account = new GeneralAccount;
        $general_account->code = $request->input('code');
        $general_account->description = $request->input('description');

        $general_account->save();

        $response = [
            'message' =>  $general_account->code . ' added to Brand!',
            'general_account' => $general_account,
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [
            'general_account' => GeneralAccount::find($id)
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255',
            'description' => 'required|string',
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }
  
        $general_account = GeneralAccount::find( $id );
        $general_account->code = $request->input('code');
        $general_account->description = $request->input('description');

        // Additional Validate If code is exist
        $general_account_code_check = GeneralAccount::where('code', $name)->where('id', '!=', $id);
        if( $general_account_code_check->count() ){
            return response()->json( [ 'errors' => [
                'code' => ['The code has already used.']
            ] ], 200 );
        }

        $general_account_description_check = GeneralAccount::where('description', $name)->where('id', '!=', $id);
        if( $general_account_code_check->count() ){
            return response()->json( [ 'errors' => [
                'description' => ['The description has already used.']
            ] ], 200 );
        }

        $general_account->save();

        $response = [
            'message' =>  $general_account->code . ' was updated!',
            'general_account' => $general_account
        ];

        return response()->json( $response, 200 );
    }

        /**
     * Delete the specified resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */

    public function destroy($id) {
        $general_account = GeneralAccount::find( $id );

        $general_account->delete();

        $response = [
            'message' => $general_account->unit . 'was deleted!',
            'general_account' => $general_account
        ];

        return response()->json( $response, 200 );

    }
}
