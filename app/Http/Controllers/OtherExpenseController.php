<?php

namespace App\Http\Controllers;

use App\OtherExpense as Expense;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class OtherExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = ($request->query('per_page')) ? $request->query('per_page') : 10 ;
        $limit = ($request->query('limit')) ? $request->query('limit') : 100 ;
        $search_key = '%'.$request->query('search_key').'%';

        $expenses = Expense::where('code', 'like', $search_key)
                        ->paginate($per_page);

        $expenses->withPath('');

        $response = [
            'message' => 'List of Other Expenses',
            'other_expenses' => $expenses
        ];
        return response()->json( $response, 200 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255|unique:other_expenses',
            'description' => 'required|string|unique:other_expenses',
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }

        $expense = new Expense;
        $expense->code = $request->input('code');
        $expense->description = $request->input('description');

        $expense->save();

        $response = [
            'message' =>  $expense->code . ' added to Other Expenses!',
            'expense' => $expense,
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [
            'expense' => Expense::find($id)
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255',
            'description' => 'required|string',
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }
  
        $expense = Expense::find( $id );
        $expense->code = $request->input('code');
        $expense->description = $request->input('description');

        // Additional Validate If code is exist
        $expense_code_check = Expense::where('code', $name)->where('id', '!=', $id);
        if( $expense_code_check->count() ){
            return response()->json( [ 'errors' => [
            'code' => ['The code has already used.']
            ] ], 200 );
        }

        $expense_description_check = Expense::where('description', $name)->where('id', '!=', $id);
        if( $expense_code_check->count() ){
            return response()->json( [ 'errors' => [
            'description' => ['The description has already used.']
            ] ], 200 );
        }

        $expense->save();

        $response = [
            'message' =>  $expense->code . ' was updated!',
            'expense' => $expense
        ];

        return response()->json( $response, 200 );
    }

        /**
     * Delete the specified resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */

    public function destroy($id) {
        $expense = Expense::find( $id );

        $expense->delete();

        $response = [
            'message' => $expense->unit . 'was deleted!',
            'expense' => $expense
        ];

        return response()->json( $response, 200 );

    }
}
