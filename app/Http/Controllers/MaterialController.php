<?php

namespace App\Http\Controllers;

use App\Material;
use App\Item;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = ($request->query('per_page')) ? $request->query('per_page') : 10 ;
        $limit = ($request->query('limit')) ? $request->query('limit') : 100 ;
        $search_key = '%'.$request->query('search_key').'%';

        $materials = Material::where('code', 'like', $search_key)
                        ->orWhere('description', 'like', $search_key)
                        ->orderBy('id', 'desc')
                        ->paginate($per_page);

        $materials->withPath('');

        $response = [
            'message' => 'List of Materials',
            'materials' => $materials
        ];
        return response()->json( $response, 200 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255|unique:materials',
            'description' => 'required|string|unique:materials',
            'unit_of_measure' => 'required|integer',
            'material_type' => 'required|integer',
            'brand' => 'required|integer'
        ]);
  
        if ($validator->fails()) {
            /* Check if Trash */
            $trash = Material::withTrashed()->where('code', $request->input('code') )->first();
            if( $trash) {
                if( $trash->trashed() ) {
                    $trash->restore();
                    return response()->json( [
                        'message' =>  $trash->description . ' was restored!',
                        'material' => $trash,
                    ], 200 );
                }
            }
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }
        
        $item = new Item([
            'brand_id' => $request->input('brand')
        ]);

        $material = new Material;
        $material->code = $request->input('code');
        $material->description = $request->input('description');
        $material->unit_of_measure_id = $request->input('unit_of_measure');
        $material->material_type_id = $request->input('material_type');
        $material->save();
        $material->items()->save( $item );

        $response = [
            'message' =>  $material->code . ' added to Materials!',
            'material' => $material,
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [
            'material' => Material::find($id)
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255',
            'description' => 'required|string',
            'unit_of_measure' => 'required|integer',
            'material_type' => 'required|integer'
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }
        
        $code = $request->input('code');
        $description = $request->input('description');

        $material = Material::find( $id );
        $material->code = $code;
        $material->description = $description;
        $material->unit_of_measure_id = $request->input('unit_of_measure');
        $material->material_type_id = $request->input('material_type');

        // Additional Validate If code is exist
        $material_code_check = Material::where('code', $code)->where('id', '!=', $id);
        if( $material_code_check->count() ){
            return response()->json( [ 'errors' => [
                'code' => ['The code has already used.']
            ] ], 200 );
        }

        $material_description_check = Material::where('description', $description)->where('id', '!=', $id);
        if( $material_code_check->count() ){
            return response()->json( [ 'errors' => [
                'description' => ['The description has already used.']
            ] ], 200 );
        }

        $material->save();

        $response = [
            'message' =>  $material->code . ' was updated!',
            'material' => $material
        ];

        return response()->json( $response, 200 );
    }

        /**
     * Delete the specified resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */

    public function destroy($id) {
        $material = Material::find( $id );

        $material->delete();

        $response = [
            'message' => $material->description . ' was deleted!',
            'material' => $material
        ];

        return response()->json( $response, 200 );

    }
}
