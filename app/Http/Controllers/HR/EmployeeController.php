<?php

namespace App\Http\Controllers\HR;

use App\HR\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class EmployeeController extends Controller
{
    /**
     * Validation array
    **/
    protected function validationArray() {
        $allowed_civil_status = [
            'SINGLE', 'MARRIED',
            'SEPARATED', 'WIDOW',
            'DIVORCED', 'ANNULLED',
            'WIDOWER', 'SINGLE PARENT'
        ];

        return [
            'last_name' => 'required',
            'first_name' => 'required',
            'middle_name' => 'required',
            'hired_date' => 'required|date',
            'birth_date' => 'required|date',
            'civil_status' => 'required|in:' . implode(',', $allowed_civil_status),
            'no_of_children' => 'integer',
            'no_of_dependent' => 'integer',
            'hdmf_ee' => 'integer',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = ($request->query('per_page')) ? $request->query('per_page') : 10 ;
        $limit = ($request->query('limit')) ? $request->query('limit') : 100 ;
        $search_key = '%'.$request->query('search_key').'%';

        $employees = Employee::where('first_name', 'like', $search_key)
                        ->orWhere('last_name', 'like', $search_key)
                        ->orderBy('last_name', 'desc');

        if( $request->query('for_list') ) {
            $employees = $employees->get();
        }else{
            $employees = $employees->paginate($per_page,['*'],'sub_page')
                                ->withPath('');
        }

        $response = [
            'message' => 'List of Employee',
            'employees' => $employees
        ];
        return response()->json( $response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request Validitaion */
        $validation_array = $this->validationArray();
        $validator = Validator::make( $request->all(), $validation_array);

        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ]);
        }
  
        $check_employee = Employee::withTrashed()
                ->where('first_name', $request->input('first_name') )
                ->where('middle_name', $request->input('middle_name') )
                ->where('last_name', $request->input('last_name') )
                ->first();

        if( $check_employee) {
            /* Check if Trash */
            if( $check_employee->trashed() ) {
                $check_employee->restore();
                return response()->json( [
                    'message' =>  $check_employee->full_name . ' was restored!',
                    'employee' => $check_employee,
                ]);
            }

            $exist_message  = $check_employee->full_name . ' was already exist!';
            return response()->json( [
                'error' =>  [
                    "last_name"=> [ $exist_message ],
                    "first_name"=> [ $exist_message ],
                    "middle_name"=> [ $exist_message ],
                ]
            ]);
        }

        $employee = new Employee( $request->all() );
        $employee->save();

        $response = [
            'message' =>  $employee->full_name . ' added to Employee!',
            'employee' => $employee,
        ];

        return response()->json( $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::find($id);
        $employee_pans = $employee->employee_pans()
                                ->paginate(10,['*'],'pan_page')
                                ->withPath('');

        $response = [
            'employee' => [
                'details' => $employee,
                'pans' => $employee_pans
            ]
        ];

        return response()->json( $response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $request Validitaion */
        $validation_array = $this->validationArray();
        $validator = Validator::make( $request->all(), $validation_array);

        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ]);
        }

        $employee = Employee::find( $id );

        // Additional Validate If code is exist
        $employee_check = Employee::where('first_name', $request->input('first_name') )
                                ->where('middle_name', $request->input('middle_name') )
                                ->where('last_name', $request->input('last_name') )
                                ->where('id', '!=', $id)
                                ->first();

        if( $employee_check  ){
            $exist_message  = $employee_check->full_name . ' was already exist!';
            return response()->json( [
                'errors' =>  [
                    "last_name"=> [ $exist_message ],
                    "first_name"=> [ $exist_message ],
                    "middle_name"=> [ $exist_message ],
                ]
            ]);
        }

        $employee->update( $request->all() );

        $response = [
            'message' =>  $employee->full_name . ' was updated!',
            'employee' => $employee
        ];

        return response()->json( $response );
    }

        /**
     * Delete the specified resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */

    public function destroy($id) {
        $employee = Employee::find( $id );

        $employee->delete();

        $response = [
            'message' => $employee->full_name . ' was removed!',
            'employee' => $employee
        ];

        return response()->json( $response);

    }
}
