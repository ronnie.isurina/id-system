<?php

namespace App\Http\Controllers\HR;

use App\HR\Employee;
use App\HR\EmployeePan as Pan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class EmployeePanController extends Controller
{
    /**
     * Validation array
    **/
    protected function validationArray() {
        return [
            'nature' => 'required',
            'department' => 'required',
            'job_title' => 'required',
            'superior' => 'required',
            'pay_class' => 'required',
            'effectivity' => 'required|date',
            'basic_salary' => 'required|integer',
            'other_allow' => 'integer',
            'rice_allow' => 'integer',
            'laundry_allow' => 'integer',
            'clothing_allow' => 'integer',
            'medical_allow' => 'integer',
            'ob_allow' => 'integer',
            'ecola' => 'integer',
            'pay_13th' => 'integer',
            'is_saved' => 'boolean',
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($empliyee_id,Request $request)
    {
        /* $request Validitaion */
        $validation_array = $this->validationArray();
        $validator = Validator::make( $request->all(), $validation_array);

        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ]);
        }

        $employee = Employee::find( $empliyee_id );

        if($employee->employee_pans->where('is_saved',false)->count()) {
            return response()->json([
                'message' =>  'You have un-saved PAN.',
                'error' => true
            ]);
        }

        $pan = new Pan( $request->all() );
        
        $employee->employee_pans()->save($pan);

        $response = [
            'message' =>  $employee->full_name . ' has added new pan!',
            'pan' => $pan,
        ];

        return response()->json( $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($employee_id, $id)
    {
        $employee = Employee::find($employee_id);
        $pan = $employee->employee_pans->where('id', $id)->first();
        $old_pan = "";
        
        if ($pan) {
            $old_pan = $employee->employee_pans->where('id', '<',$id)->first();
        }

        $response = [
            'data' => [ 'pan' => $pan, 'old_pan' => $old_pan ]
        ];

        return response()->json( $response );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $employee_id employee_id
     * @param  int  $id pan id
     * @return \Illuminate\Http\Response
     */
    public function update($employee_id, $id, Request $request)
    {
        /* $request Validitaion */
        $validation_array = $this->validationArray();
        $validator = Validator::make( $request->all(), $validation_array);

        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ]);
        }

        $pan = Pan::find( $id );

        if($pan->is_saved) {
            return response()->json([
                'message' =>  'This PAN already saved.<br>Can\'t update.',
                'error' => true
            ]);
        }

        $request->except(['is_save', 'id']);
        $pan->update( $request->all() );

        $response = [
            'message' =>  $pan->employee->full_name . ' PAN was updated!',
            'pan' => $pan
        ];

        return response()->json( $response );
    }

        /**
     * Delete the specified resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */

    public function destroy($id) {
        $pan = Pan::find( $id );

        $pan->delete();

        $response = [
            'message' => $pan->full_name . ' was removed!',
            'pan' => $pan
        ];

        return response()->json( $response);

    }

        /**
     * Delete the specified resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */

    public function save( $employee_id, $id ) {
        $pan = Pan::find( $id );
        $pan->is_saved = true;
        $pan->save();

        $response = [
            'message' => $pan->employee->full_name . ' was saved!',
            'pan' => $pan
        ];

        return response()->json( $response);

    }
}
