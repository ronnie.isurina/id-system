<?php

namespace App\Http\Controllers\HR;

use App\HR\Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class AgentController extends Controller
{
    /**
     * Validation array
    **/
    protected function validationArray() {
        return [
            'last_name' => 'required',
            'first_name' => 'required',
            'middle_name' => 'required',
            'started_date' => 'required|date',
            'email' => 'email'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = ($request->query('per_page')) ? $request->query('per_page') : 10 ;
        $limit = ($request->query('limit')) ? $request->query('limit') : 100 ;
        $search_key = '%'.$request->query('search_key').'%';

        $agents = Agent::where('first_name', 'like', $search_key)
                        ->orWhere('last_name', 'like', $search_key)
                        ->orderBy('last_name', 'desc')
                        ->paginate($per_page);

        $agents->withPath('');

        $response = [
            'message' => 'List of Agent',
            'agents' => $agents
        ];
        return response()->json( $response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request Validitaion */
        $validation_array = $this->validationArray();
        $validator = Validator::make( $request->all(), $validation_array);

        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ]);
        }
  
        $check_agent = Agent::withTrashed()
                ->where('first_name', $request->input('first_name') )
                ->where('middle_name', $request->input('middle_name') )
                ->where('last_name', $request->input('last_name') )
                ->first();

        if( $check_agent) {
            /* Check if Trash */
            if( $check_agent->trashed() ) {
                $check_agent->restore();
                return response()->json( [
                    'message' =>  $check_agent->full_name . ' was restored!',
                    'agent' => $check_agent,
                ]);
            }

            $exist_message  = $check_agent->full_name . ' was already exist!';
            return response()->json( [
                'error' =>  [
                    "last_name"=> [ $exist_message ],
                    "first_name"=> [ $exist_message ],
                    "middle_name"=> [ $exist_message ],
                ]
            ]);
        }

        $agent = new Agent( $request->all() );
        $agent->save();

        $response = [
            'message' =>  $agent->full_name . ' added to agent!',
            'agent' => $agent,
        ];

        return response()->json( $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [
            'agent' => Agent::find($id)
        ];

        return response()->json( $response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $request Validitaion */
        $validation_array = $this->validationArray();
        $validator = Validator::make( $request->all(), $validation_array);

        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ]);
        }

        $agent = Agent::find( $id );

        // Additional Validate If code is exist
        $agent_check = Agent::where('first_name', $request->input('first_name') )
                                ->where('middle_name', $request->input('middle_name') )
                                ->where('last_name', $request->input('last_name') )
                                ->where('id', '!=', $id)
                                ->first();

        if( $agent_check  ){
            $exist_message  = $agent_check->full_name . ' was already exist!';
            return response()->json( [
                'error' =>  [
                    "last_name"=> [ $exist_message ],
                    "first_name"=> [ $exist_message ],
                    "middle_name"=> [ $exist_message ],
                ]
            ]);
        }

        $agent->update( $request->all() );

        $response = [
            'message' =>  $agent->full_name . ' was updated!',
            'agent' => $agent
        ];

        return response()->json( $response );
    }

        /**
     * Delete the specified resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */

    public function destroy($id) {
        $agent = Agent::find( $id );

        $agent->delete();

        $response = [
            'message' => $agent->full_name . ' was removed!',
            'agent' => $agent
        ];

        return response()->json( $response);

    }
}
