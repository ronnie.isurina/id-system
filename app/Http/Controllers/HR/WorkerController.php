<?php

namespace App\Http\Controllers\HR;

use App\HR\Worker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class WorkerController extends Controller
{
    /**
     * Validation array
    **/
    protected function validationArray() {
        return [
            'last_name' => 'required',
            'first_name' => 'required',
            'middle_name' => 'required',
            'hired_date' => 'required|date',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = ($request->query('per_page')) ? $request->query('per_page') : 10 ;
        $limit = ($request->query('limit')) ? $request->query('limit') : 100 ;
        $search_key = '%'.$request->query('search_key').'%';

        $workers = Worker::where('first_name', 'like', $search_key)
                        ->orWhere('last_name', 'like', $search_key)
                        ->orderBy('last_name', 'desc')
                        ->paginate($per_page);

        $workers->withPath('');

        $response = [
            'message' => 'List of Worker',
            'workers' => $workers
        ];
        return response()->json( $response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request Validitaion */
        $validation_array = $this->validationArray();
        $validator = Validator::make( $request->all(), $validation_array);

        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ]);
        }
  
        $check_worker = Worker::withTrashed()
                ->where('first_name', $request->input('first_name') )
                ->where('middle_name', $request->input('middle_name') )
                ->where('last_name', $request->input('last_name') )
                ->first();

        if( $check_worker) {
            /* Check if Trash */
            if( $check_worker->trashed() ) {
                $check_worker->restore();
                return response()->json( [
                    'message' =>  $check_worker->full_name . ' was restored!',
                    'worker' => $check_worker,
                ]);
            }

            $exist_message  = $check_worker->full_name . ' was already exist!';
            return response()->json( [
                'error' =>  [
                    "last_name"=> [ $exist_message ],
                    "first_name"=> [ $exist_message ],
                    "middle_name"=> [ $exist_message ],
                ]
            ]);
        }

        $worker = new Worker( $request->all() );
        $worker->save();

        $response = [
            'message' =>  $worker->full_name . ' added to worker!',
            'worker' => $worker,
        ];

        return response()->json( $response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [
            'worker' => Worker::find($id)
        ];

        return response()->json( $response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $request Validitaion */
        $validation_array = $this->validationArray();
        $validator = Validator::make( $request->all(), $validation_array);

        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ]);
        }

        $worker = Worker::find( $id );

        // Additional Validate If code is exist
        $worker_check = Worker::where('first_name', $request->input('first_name') )
                                ->where('middle_name', $request->input('middle_name') )
                                ->where('last_name', $request->input('last_name') )
                                ->where('id', '!=', $id)
                                ->first();

        if( $worker_check  ){
            $exist_message  = $worker_check->full_name . ' was already exist!';
            return response()->json( [
                'error' =>  [
                    "last_name"=> [ $exist_message ],
                    "first_name"=> [ $exist_message ],
                    "middle_name"=> [ $exist_message ],
                ]
            ]);
        }

        $worker->update( $request->all() );

        $response = [
            'message' =>  $worker->full_name . ' was updated!',
            'worker' => $worker
        ];

        return response()->json( $response );
    }

        /**
     * Delete the specified resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */

    public function destroy($id) {
        $worker = Worker::find( $id );

        $worker->delete();

        $response = [
            'message' => $worker->full_name . ' was removed!',
            'worker' => $worker
        ];

        return response()->json( $response);

    }
}
