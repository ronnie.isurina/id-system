<?php

namespace App\Http\Controllers;

use App\Item;
use App\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($type, $type_id, Request $request)
    {   
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'brand' => 'required|integer',
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }

        $item = new Item;
        $brand = Brand::find( $request->input('brand') );

        /* Validate Brand */
        if( !$brand) {
            return response()->json( [
                'errors' => [
                    'brand'=> ['This brand not exist.']
                ]
            ]);
        }

        $item->brand_id = $brand->id;
        $morph = $this->itemType( $type, $type_id );

        if (!$morph) {
            return response()->json( [
                'errors' => ucfirst($type) . ' item was not exist.'
            ]);
        }

        #Validate
        $response = $this->validateExistence($morph, $brand);
        if($response) {
            return response()->json( $response );
        }

        $morph->items()->save( $item );

        $response = [
            'message' =>  $brand->code . ' added as brand of '. $morph->description .'!',
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Item Type Selection
     * @param string, integer
     * @return Illuminate\Database\Eloquent\Model
    */
    public function itemType( $type, $type_id ) {
        $morph = '';
        switch ( $type ) {
            case 'material':
                $morph = \App\Material::find( $type_id );
                break;
            case 'tool':
                $morph = \App\Tool::find( $type_id );
                break;
            case 'equipment':
                $morph = \App\Equipment::find( $type_id );
                break;
            default:
                break;
        }
        return $morph;
    }

    /**
     * Validate of brand existence
     * @param Illuminate\Database\Eloquent\Model
     * @param Illuminate\Database\Eloquent\Model
     * @return array
    **/
    public function validateExistence($morph,$brand) {
        $response = '';
        $item = $morph->items()->withTrashed()->where('brand_id', $brand->id)->first();
        if($item) {
            if( $item->trashed() ) {
                $item->restore();
                $response = [
                    'message' =>  $item->brand->code . ' was restored!',
                    'item' => $item,
                ];
            }else{
                $response = [
                    'message' =>  $item->brand->code . ' was alread exist!',
                    'item' => $item,
                ];
            }
        }
        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::find( $id );
        
        $morph = $this->itemType( $item->items_type, $item->items_id );

        if( $morph->items()->count() <= 1 ) {
            return response()->json( [
                'message' =>  'Can\'t delete! <br> Please add first!',
            ]);
        }
        
        if(!$item){
            return response()->json([
                'message' =>  'Already remove!',
            ]);
        }

        $item->delete();

        $response = [
            'message' =>  $item->brand->code . ' was remove!',
        ];
        
        return response()->json( $response, 200 );
    }
}
