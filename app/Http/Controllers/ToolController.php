<?php

namespace App\Http\Controllers;

use App\Tool;
use App\Item;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class ToolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = ($request->query('per_page')) ? $request->query('per_page') : 10 ;
        $limit = ($request->query('limit')) ? $request->query('limit') : 100 ;
        $search_key = '%'.$request->query('search_key').'%';

       $tools = Tool::where('code', 'like', $search_key)
                        ->orWhere('description', 'like', $search_key)
                        ->orderBy('id', 'desc')
                        ->paginate($per_page);

       $tools->withPath('');

        $response = [
            'message' => 'List of Tool',
            'tools' =>$tools
        ];
        return response()->json( $response, 200 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255|unique:tools',
            'description' => 'required|string|unique:tools',
            'brand' => 'required|integer'
        ]);
  
        if ($validator->fails()) {
            /* Check if Trash */
            $trash = Tool::withTrashed()->where('code', $request->input('code') )->first();
            if($trash) {
                if( $trash->trashed() ) {
                    $trash->restore();
                    return response()->json( [
                        'message' =>  $trash->description . ' was restored!',
                        'equipment' => $trash,
                    ], 200 );
                }
            }
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }
        
        $item = new Item([
            'brand_id' => $request->input('brand')
        ]);

        $tool = new Tool;
        $tool->code = $request->input('code');
        $tool->description = $request->input('description');
        $tool->save();
        $tool->items()->save( $item );

        $response = [
            'message' =>  $tool->code . ' added to Tools!',
            'tool' => $tool,
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [
            'tool' => Tool::find($id)
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255',
            'description' => 'required|string',
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }
        
        $code = $request->input('code');
        $description = $request->input('description');

        $tool = Tool::find( $id );
        $tool->code = $code;
        $tool->description = $description;

        // Additional Validate If code is exist
        $tool_code_check = Tool::where('code', $code)->where('id', '!=', $id);
        if( $tool_code_check->count() ){
            return response()->json( [ 'errors' => [
                'code' => ['The code has already used.']
            ] ], 200 );
        }

        $tool_description_check = Tool::where('description', $description)->where('id', '!=', $id);
        if( $tool_code_check->count() ){
            return response()->json( [ 'errors' => [
                'description' => ['The description has already used.']
            ] ], 200 );
        }

        $tool->save();

        $response = [
            'message' =>  $tool->code . ' was updated!',
            'equipment' => $tool
        ];

        return response()->json( $response, 200 );
    }

        /**
     * Delete the specified resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */

    public function destroy($id) {
        $tool = Tool::find( $id );

        $tool->delete();

        $response = [
            'message' => $tool->description . ' was deleted!',
            'tool' => $tool
        ];

        return response()->json( $response, 200 );

    }
}
