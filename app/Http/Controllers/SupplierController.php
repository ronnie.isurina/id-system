<?php

namespace App\Http\Controllers;

use App\Supplier;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = ($request->query('per_page')) ? $request->query('per_page') : 10 ;
        $limit = ($request->query('limit')) ? $request->query('limit') : 100 ;
        $search_key = '%'.$request->query('search_key').'%';

        $suppliers = Supplier::where('code', 'like', $search_key)
                        ->paginate($per_page);

        $suppliers->withPath('');

        $response = [
            'message' => 'List of Suppliers',
            'suppliers' => $suppliers
        ];
        return response()->json( $response, 200 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255|unique:suppliers',
            'name' => 'required|string|unique:suppliers',
            'address' => 'required|max:255',
        ]);
  
        if ($validator->fails()) {
            /* Check if Trash */
            $trash = Supplier::withTrashed()->where('code', $request->input('code') )->first();
            if( $trash) {
                if( $trash->trashed() ) {
                    $trash->restore();
                    return response()->json( [
                        'message' =>  $trash->name . ' was restored!',
                        'supplier' => $trash,
                    ], 200 );
                }
            }
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }

        $supplier = new Supplier;
        $supplier->code = $request->input('code');
        $supplier->name = $request->input('name');
        $supplier->address = $request->input('address');
        $supplier->tel_no = $request->input('tel_no');
        $supplier->cel_no = $request->input('cel_no');
        $supplier->contact_person = $request->input('contact_person');
        $supplier->terms = $request->input('terms');
        $supplier->bank_account = $request->input('bank_account');
        $supplier->bank = $request->input('bank');
        $supplier->branch = $request->input('branch');
        $supplier->remarks = $request->input('remarks');

        $supplier->save();

        $response = [
            'message' =>  $supplier->name . ' added to Suppliers!',
            'supplier' => $supplier,
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [
            'supplier' => Supplier::find($id)
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255',
            'name' => 'required|string',
            'address' => 'required|max:255',
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }
        $code = $request->input('code');
        $name = $request->input('name');

        $supplier = Supplier::find( $id );
        $supplier->code = $code;
        $supplier->name = $name;
        $supplier->address = $request->input('address');
        $supplier->tel_no = $request->input('tel_no');
        $supplier->cel_no = $request->input('cel_no');
        $supplier->contact_person = $request->input('contact_person');
        $supplier->terms = $request->input('terms');
        $supplier->bank_account = $request->input('bank_account');
        $supplier->bank = $request->input('bank');
        $supplier->branch = $request->input('branch');
        $supplier->remarks = $request->input('remarks');

        // Additional Validate If code is exist
        $supplier_code_check = Supplier::where('code', $code)->where('id', '!=', $id);
        if( $supplier_code_check->count() ){
            return response()->json( [ 'errors' => [
            'code' => ['The code has already used.']
            ] ], 200 );
        }

        $supplier_name_check = Supplier::where('name', $name)->where('id', '!=', $id);
        if( $supplier_code_check->count() ){
            return response()->json( [ 'errors' => [
            'name' => ['The name has already used.']
            ] ], 200 );
        }

        $supplier->save();

        $response = [
            'message' =>  $supplier->name . ' was updated!',
            'supplier' => $supplier
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Delete the specified resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */

    public function destroy($id) {
        $supplier = Supplier::find( $id );

        $supplier->delete();

        $response = [
            'message' => $supplier->name . ' was deleted!',
            'supplier' => $supplier
        ];

        return response()->json( $response, 200 );

    }
}
