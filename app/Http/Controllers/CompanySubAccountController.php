<?php

namespace App\Http\Controllers;

use App\CompanySubAccount;
use App\CompanyAccount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CompanySubAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = ($request->query('sub_per_page')) ? $request->query('sub_per_page') : 10 ;
        $limit = ($request->query('sub_limit')) ? $request->query('sub_limit') : 100 ;
        $search_key = '%'.$request->query('sub_search_key').'%';

        $company_sub_accounts = CompanySubAccount::where('code', 'like', $search_key)
                        ->orderBy('id', 'desc')
                        ;

        $company_sub_accounts = ($request->query('for_list')) 
                                ? $company_sub_accounts->get()
                                : $company_sub_accounts->paginate($per_page,['*'],'sub_page')
                                                            ->withPath('');

        $response = [
            'message' => 'List of Company Sub Accounts',
            'company_sub_accounts' => $company_sub_accounts
        ];
        return response()->json( $response, 200 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255|unique:company_sub_accounts',
            'description' => 'required|string|unique:company_sub_accounts',
            'company_account' => 'required|integer',
        ]);
  
        if ($validator->fails()) {
            /* Check if Trash */
            $trash_company_sub_account = CompanySubAccount::withTrashed()->where('code', $request->input('code') )->first();
            if( $trash_company_sub_account ) {
                if( $trash_company_sub_account->trashed() ) {
                    $trash_company_sub_account->restore();
                    return response()->json( [
                        'message' =>  $trash_company_sub_account->code . ' was restored!',
                        'company_sub_account' => $trash_company_sub_account,
                    ], 200 );
                }
            }
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }

        $company_sub_account = new CompanySubAccount;
        $company_sub_account->code = $request->input('code');
        $company_sub_account->description = $request->input('description');
        $company_sub_account->company_account_id = $request->input('company_account');

        $company_sub_account->save();

        $response = [
            'message' =>  $company_sub_account->code . ' added to Company Sub Account!',
            'company_sub_account' => $company_sub_account,
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [
            'company_sub_account' => CompanySubAccount::find($id)
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255',
            'description' => 'required|string',
            'company_account' => 'required|integer',
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }

        $code = $request->input('code');

        $company_sub_account = CompanySubAccount::find( $id );
        $company_sub_account->code = $code;
        $company_sub_account->description = $request->input('description');
        $company_sub_account->company_account_id = $request->input('company_account');

        // Additional Validate If code is exist
        $company_sub_account_code_check = CompanySubAccount::where('code', $code)->where('id', '!=', $id);
        if( $company_sub_account_code_check->count() ){
            return response()->json( [ 'errors' => [
            'code' => ['The code has already used.']
            ] ], 200 );
        }

        $company_sub_account_description_check = CompanySubAccount::where('description', $code)->where('id', '!=', $id);
        if( $company_sub_account_code_check->count() ){
            return response()->json( [ 'errors' => [
            'description' => ['The description has already used.']
            ] ], 200 );
        }

        $company_sub_account->save();

        $response = [
            'message' =>  $company_sub_account->code . ' was updated!',
            'company_sub_account' => $company_sub_account
        ];

        return response()->json( $response, 200 );
    }

     /**
     * Delete the specified resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */

    public function destroy($id) {
        $company_sub_account = CompanySubAccount::find( $id );

        $company_sub_account->delete();

        $response = [
            'message' => $company_sub_account->code . ' was deleted!',
            'errors' => false,
            'company_sub_account' => $company_sub_account
        ];

        return response()->json( $response, 200 );

    }
}
