<?php

namespace App\Http\Controllers;

use App\Supply;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class SupplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = ($request->query('per_page')) ? $request->query('per_page') : 10 ;
        $limit = ($request->query('limit')) ? $request->query('limit') : 100 ;
        $search_key = '%'.$request->query('search_key').'%';

        $supplies = Supply::where('code', 'like', $search_key)
                        ->paginate($per_page);

        $supplies->withPath('');

        $response = [
            'message' => 'List of Supplies',
            'supplies' => $supplies
        ];
        return response()->json( $response, 200 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255|unique:supplies',
            'description' => 'required|string|unique:supplies',
        ]);
  
        if ($validator->fails()) {
            /* Check if Trash */
            $trash_supply = Supply::withTrashed()->where('code', $request->input('code') )->first();
            if( $trash_supply ) {
                if( $trash_supply->trashed() ) {
                    $trash_supply->restore();
                    return response()->json( [
                        'message' =>  $trash_supply->code . ' was restored!',
                        'supply' => $trash_supply,
                    ], 200 );
                }
            }
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }

        $supply = new Supply;
        $supply->code = $request->input('code');
        $supply->description = $request->input('description');

        $supply->save();

        $response = [
            'message' =>  $supply->code . ' added to Utilities!',
            'supply' => $supply,
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [
            'supply' => Supply::find($id)
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255',
            'description' => 'required|string',
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }
        $code = $request->input('code');

        $supply = Supply::find( $id );
        $supply->code = $code;
        $supply->description = $request->input('description');

        // Additional Validate If code is exist
        $supply_code_check = Supply::where('code', $code)->where('id', '!=', $id);
        if( $supply_code_check->count() ){
            return response()->json( [ 'errors' => [
            'code' => ['The code has already used.']
            ] ], 200 );
        }

        $supply_description_check = Supply::where('description', $code)->where('id', '!=', $id);
        if( $supply_code_check->count() ){
            return response()->json( [ 'errors' => [
            'description' => ['The description has already used.']
            ] ], 200 );
        }

        $supply->save();

        $response = [
            'message' =>  $supply->code . ' was updated!',
            'supply' => $supply
        ];

        return response()->json( $response, 200 );
    }

        /**
     * Delete the specified resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */

    public function destroy($id) {
        $supply = Supply::find( $id );

        $supply->delete();

        $response = [
            'message' => $supply->code . ' was deleted!',
            'errors' => false,
            'supply' => $supply
        ];

        return response()->json( $response, 200 );

    }
}
