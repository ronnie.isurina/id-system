<?php

namespace App\Http\Controllers;

use App\MaterialType as Type;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class MaterialTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = ($request->query('per_page')) ? $request->query('per_page') : 10 ;
        $limit = ($request->query('limit')) ? $request->query('limit') : 100 ;
        $search_key = '%'.$request->query('search_key').'%';

        $material_types = Type::where('code', 'like', $search_key)
                        ->orWhere('description', 'like', $search_key)
                        ->paginate($per_page);

        $material_types->withPath('');

        $response = [
            'message' => 'List of Material Type',
            'material_types' => $material_types
        ];
        return response()->json( $response, 200 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255|unique:material_types',
            'description' => 'required|string|max:255',
        ]);
  
        if ($validator->fails()) {
            /* Check if Trash */
            $trash_material_type = Type::withTrashed()->where('code', $request->input('code') )->first();
            if( $trash_material_type) {
                if( $trash_material_type->trashed() ) {
                    $trash_material_type->restore();
                    return response()->json( [
                        'message' =>  $trash_material_type->code . ' was restored!',
                        'material_type' => $trash_material_type,
                    ], 200 );
                }
            }
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }

        $material_type = new Type;
        $material_type->code = $request->input('code');
        $material_type->description = $request->input('description');
        $material_type->save();

        $response = [
            'message' =>  $material_type->code . ' added to Material Type!',
            'material_type' => $material_type
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [
            'material_type' => Type::find($id)
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string',
            'description' => 'required|string',
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }
  
        $material_type = Type::find( $id );
        $material_type->code = $request->input('code');
        $material_type->description = $request->input('description');
        $material_type->save();

        $response = [
            'message' =>  $material_type->code . ' was updates!',
            'material_type' => $material_type
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Delete the specified resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */

    public function destroy($id) {
        $material_type = Type::find( $id );

        $material_type->delete();

        $response = [
            'message' => $material_type->code . 'was delete!',
            'material_type' => $material_type
        ];

        return response()->json( $response, 200 );

    }
}