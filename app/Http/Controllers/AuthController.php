<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    protected $maxAttempts=5;
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT token via given credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make( $request->all(), [
          'email' => 'required',
          'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json( [ 'error' => $validator->errors() ], 200 );
        }
          
        $credentials = [
            'email' => $request->input('email'),
            'password' => $request->input('password'),
            'is_active' => true
        ];

        $user = User::where('email', $request->input('email'))->first();
        if( !$user->is_active ) {
            return response()->json([ 
                'error' => [
                    'message' => 'Your account was disabled!'
                ]
            ]);
        }

        if ($token = $this->guard()->attempt($credentials)) {
            $this->clearLoginAttempts($request);
            return $this->respondWithToken($token);
        }

        # Throttles Logins
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            
            $user->is_active = false;
            $user->save();

            return response()->json([ 
                'error' => [
                    'message' => 'Your account disabled after consecutive failed!'
                ]
            ]);
        }

        $this->incrementLoginAttempts($request);

        return response()->json([ 
            'error' => [
                'message' => 'Invalid email or password!'
            ] 
        ], 200);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json($this->guard()->user());
    }

    /**
     * Log the user out (Invalidate the token)
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $this->guard()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        $ttl = $this->guard()->factory()->getTTL();
        
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => strtotime( "+{$ttl} minutes" ),
        ]);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\Guard
     */
    public function guard()
    {
        return Auth::guard();
    }
}