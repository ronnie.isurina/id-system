<?php

namespace App\Http\Controllers;

use App\Utility;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class UtilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = ($request->query('per_page')) ? $request->query('per_page') : 10 ;
        $limit = ($request->query('limit')) ? $request->query('limit') : 100 ;
        $search_key = '%'.$request->query('search_key').'%';

        $utilities = Utility::where('code', 'like', $search_key)
                        ->paginate($per_page);

        $utilities->withPath('');

        $response = [
            'message' => 'List of Utilities',
            'utilities' => $utilities
        ];
        return response()->json( $response, 200 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255|unique:utilities',
            'description' => 'required|string|unique:utilities',
        ]);
  
        if ($validator->fails()) {
            /* Check if Trash */
            $trash_utility = Utility::withTrashed()->where('code', $request->input('code') )->first();
            if( $trash_utility ) {
                if( $trash_utility->trashed() ) {
                    $trash_utility->restore();
                    return response()->json( [
                        'message' =>  $trash_utility->code . ' was restored!',
                        'utility' => $trash_utility,
                    ], 200 );
                }
            }
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }

        $utility = new Utility;
        $utility->code = $request->input('code');
        $utility->description = $request->input('description');

        $utility->save();

        $response = [
            'message' =>  $utility->code . ' added to Utilities!',
            'utility' => $utility,
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [
            'utility' => Utility::find($id)
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255',
            'description' => 'required|string',
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }
        $code = $request->input('code');

        $utility = Utility::find( $id );
        $utility->code = $code;
        $utility->description = $request->input('description');

        // Additional Validate If code is exist
        $utility_code_check = Utility::where('code', $code)->where('id', '!=', $id);
        if( $utility_code_check->count() ){
            return response()->json( [ 'errors' => [
            'code' => ['The code has already used.']
            ] ], 200 );
        }

        $utility_description_check = Utility::where('description', $code)->where('id', '!=', $id);
        if( $utility_code_check->count() ){
            return response()->json( [ 'errors' => [
            'description' => ['The description has already used.']
            ] ], 200 );
        }

        $utility->save();

        $response = [
            'message' =>  $utility->code . ' was updated!',
            'utility' => $utility
        ];

        return response()->json( $response, 200 );
    }

        /**
     * Delete the specified resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */

    public function destroy($id) {
        $utility = Utility::find( $id );

        $utility->delete();

        $response = [
            'message' => $utility->code . ' was deleted!',
            'errors' => false,
            'utility' => $utility
        ];

        return response()->json( $response, 200 );

    }
}
