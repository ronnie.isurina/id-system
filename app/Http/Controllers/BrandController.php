<?php

namespace App\Http\Controllers;

use App\Brand as Brand;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = ($request->query('per_page')) ? $request->query('per_page') : 10 ;
        $limit = ($request->query('limit')) ? $request->query('limit') : 100 ;
        $search_key = '%'.$request->query('search_key').'%';

        $brands = Brand::where('code', 'like', $search_key)
                        ->paginate($per_page);

        $brands->withPath('');

        $response = [
            'message' => 'List of Brands',
            'brands' => $brands
        ];
        return response()->json( $response, 200 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255|unique:brands',
            'description' => 'required|string|unique:brands',
        ]);
  
        if ($validator->fails()) {
            /* Check if Trash */
            $trash = Brand::withTrashed()->where('code', $request->input('code') )->first();
            if( $trash) {
                if( $trash->trashed() ) {
                    $trash->restore();
                    return response()->json( [
                        'message' =>  $trash->code . ' was restored!',
                        'brand' => $trash,
                    ], 200 );
                }
            }
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }

        $brand = new Brand;
        $brand->code = $request->input('code');
        $brand->description = $request->input('description');

        $brand->save();

        $response = [
            'message' =>  $brand->code . ' added to Brand!',
            'brand' => $brand,
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [
            'brand' => Brand::find($id)
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255',
            'description' => 'required|string',
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }
        $code = $request->input('code');

        $brand = Brand::find( $id );
        $brand->code = $code;
        $brand->description = $request->input('description');

        // Additional Validate If code is exist
        $brand_code_check = Brand::where('code', $code)->where('id', '!=', $id);
        if( $brand_code_check->count() ){
            return response()->json( [ 'errors' => [
            'code' => ['The code has already used.']
            ] ], 200 );
        }

        $brand_description_check = Brand::where('description', $code)->where('id', '!=', $id);
        if( $brand_code_check->count() ){
            return response()->json( [ 'errors' => [
            'description' => ['The description has already used.']
            ] ], 200 );
        }

        $brand->save();

        $response = [
            'message' =>  $brand->code . ' was updated!',
            'brand' => $brand
        ];

        return response()->json( $response, 200 );
    }

        /**
     * Delete the specified resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */

    public function destroy($id) {
        $brand = Brand::find( $id );

        $brand->delete();

        $response = [
            'message' => $brand->code . ' was deleted!',
            'brand' => $brand
        ];

        return response()->json( $response, 200 );

    }
}
