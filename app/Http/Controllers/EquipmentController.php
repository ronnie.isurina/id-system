<?php

namespace App\Http\Controllers;

use App\Equipment;
use App\Item;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class EquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = ($request->query('per_page')) ? $request->query('per_page') : 10 ;
        $limit = ($request->query('limit')) ? $request->query('limit') : 100 ;
        $search_key = '%'.$request->query('search_key').'%';

        $equipments = Equipment::where('code', 'like', $search_key)
                        ->orWhere('description', 'like', $search_key)
                        ->orderBy('id', 'desc')
                        ->paginate($per_page);

        $equipments->withPath('');

        $response = [
            'message' => 'List of Equipment',
            'equipments' => $equipments
        ];
        return response()->json( $response, 200 );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255|unique:equipments',
            'description' => 'required|string|unique:equipments',
            'brand' => 'required|integer'
        ]);
  
        if ($validator->fails()) {
            /* Check if Trash */
            $trash = Equipment::withTrashed()->where('code', $request->input('code') )->first();
            if($trash) {
                if( $trash->trashed() ) {
                    $trash->restore();
                    return response()->json( [
                        'message' =>  $trash->description . ' was restored!',
                        'equipment' => $trash,
                    ], 200 );
                }
            }
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }
        
        $item = new Item([
            'brand_id' => $request->input('brand')
        ]);

        $equipment = new Equipment;
        $equipment->code = $request->input('code');
        $equipment->description = $request->input('description');
        $equipment->save();
        $equipment->items()->save( $item );

        $response = [
            'message' =>  $equipment->code . ' added to Equipments!',
            'equipment' => $equipment,
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = [
            'equipment' => Equipment::find($id)
        ];

        return response()->json( $response, 200 );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /* $request Validitaion */
        $validator = Validator::make( $request->all(), [
            'code' => 'required|string|max:255',
            'description' => 'required|string',
        ]);
  
        if ($validator->fails()) {
            return response()->json( [ 'errors' => $validator->errors() ], 200 );
        }
        
        $code = $request->input('code');
        $description = $request->input('description');

        $equipment = Equipment::find( $id );
        $equipment->code = $code;
        $equipment->description = $description;

        // Additional Validate If code is exist
        $equipment_code_check = Equipment::where('code', $code)->where('id', '!=', $id);
        if( $equipment_code_check->count() ){
            return response()->json( [ 'errors' => [
                'code' => ['The code has already used.']
            ] ], 200 );
        }

        $equipment_description_check = Equipment::where('description', $description)->where('id', '!=', $id);
        if( $equipment_code_check->count() ){
            return response()->json( [ 'errors' => [
                'description' => ['The description has already used.']
            ] ], 200 );
        }

        $equipment->save();

        $response = [
            'message' =>  $equipment->code . ' was updated!',
            'equipment' => $equipment
        ];

        return response()->json( $response, 200 );
    }

        /**
     * Delete the specified resource in storage
     * 
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response 
     */

    public function destroy($id) {
        $equipment = Equipment::find( $id );

        $equipment->delete();

        $response = [
            'message' => $equipment->description . ' was deleted!',
            'equipment' => $equipment
        ];

        return response()->json( $response, 200 );

    }
}
