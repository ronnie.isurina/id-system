<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnitOfMeasure extends Model
{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];

    /**
     * Get all of the materials.
    */
    public function materials()
    {
        return $this->hasMany('App\Material');
    }
}
