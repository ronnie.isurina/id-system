<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanySubAccount extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $appends = ['company_account'];

    /**
     * Get all of the chart of account item.
    */
    public function company_account()
    {
        return $this->belongsTo('App\CompanyAccount');
    }

    public function getCompanyAccountAttribute() {
        $company_account = $this->company_account()->first();
        return collect($company_account)->except(['id','chart_of_account_id','deleted_at', 'created_at', 'updated_at'])->toArray();
    }
}
