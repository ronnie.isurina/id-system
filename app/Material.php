<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Material extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $appends = [
        'brands', 'material_type', 'unit_of_measure'
    ];

    /**
     * Material type.
    */
    public function material_type()
    {
        return $this->belongsTo('App\MaterialType');
    }

    /**
     * Unit Of measure.
    */
    public function unit_of_measure()
    {
        return $this->belongsTo('App\UnitOfMeasure');
    }

    /**
     * Item Refer To Brand
     * Get all of the material's item.
    */
    public function items()
    {
        return $this->morphMany('App\Item', 'items');
    }

    public function getBrandsAttribute() {
        $items = $this->items()->get();
        
        $brands = [];
        foreach( $items as $item) {
            $brands[] = [
                'id' => $item->id,
                'brand_id' => $item->id, 
                'code' => $item->brand->code,
                'description' => $item->brand->description
            ];
        }

        return $brands;  
    }

    public function getMaterialTypeAttribute() {
        $material_type = $this->material_type()->first();
        return collect($material_type)->except(['id','deleted_at', 'created_at', 'updated_at'])->toArray();
    }

    public function getUnitOfMeasureAttribute() {
        $uoms = $this->unit_of_measure()->first();
        return collect($uoms)->except(['id','deleted_at', 'created_at', 'updated_at'])->toArray();
    }
}
