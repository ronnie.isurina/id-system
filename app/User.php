<?php

namespace App;

use App\Role;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject {
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'roles'
    ];

    // Rest omitted for brevity
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /*
    Roles relation to user
    */
    public function roles() 
    {
        return $this->belongsToMany('App\Role');
    }

    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }       
        }
        return false;
    }
    
    public function hasRole($role)
    {
        if ($this->roles()->where('role_id', $role)->first()) {
            return true;
        }
        return false;
    }

    public function getRolesAttribute()
    {
        $roles = Role::all();
        $user_role = [];

        foreach( $roles as $role) {
            $user_role [$role->group ][] = [
                'name' => $role->name,
                'description' => $role->description,
                'role_id' => $role->id,
                'has_role' => $this->hasRole( $role->id )
            ];
        }

        $filtered_role = [];
        foreach( $user_role as $key_role => $role ) {
            $filtered_role[] = [ 'group' => $key_role,
                                 'role' => $role
            ];
        }

        return $filtered_role;
    }
}
