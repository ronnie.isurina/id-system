<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MaterialType extends Model
{
    use SoftDeletes;
    
    protected $dates = ['deleted_at'];

    /**
     * Get all of the material's item.
    */
    public function materials()
    {
        return $this->hasMany('App\Material');
    }
}