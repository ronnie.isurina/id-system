<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Equipment extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'equipments';

    protected $appends = [ 'brands' ];
    /**
     * Item Refer To Brand
     * Get all of the material's item.
    */
    public function items()
    {
        return $this->morphMany('App\Item', 'items');
    }

    public function getBrandsAttribute() {
        $items = $this->items()->get();
        
        $brands = [];
        foreach( $items as $item) {
            $brands[] = [
                'id' => $item->id,
                'brand_id' => $item->id, 
                'code' => $item->brand->code,
                'description' => $item->brand->description
            ];
        }

        return $brands;  
    }
}
