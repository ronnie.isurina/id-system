<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChartOfAccount extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
    */
    public function general_account()
    {
        return $this->belongsTo('App\GeneralAccount');
    }

    /**
    */
    public function company_accounts()
    {
        return $this->hasMany('App\ChartOfAccount');
    }
}
