<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyAccount extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * Get all of the chart of account item.
    */
    public function chart_of_account()
    {
        return $this->belongsTo('App\ChartOfAccount');
    }

    /**
    */
    public function company_sub_accounts()
    {
        return $this->hasMany('App\CompanySubAccount');
    }
}
