<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run()
		{
      $roles = [
        [
          'group' => 'USER ROLE',
          'name' => 'GET USER DATA',
          'description' => 'SHOW USER DATA'
        ],
        [
          'group' => 'USER ROLE',
          'name' => 'ADD USER',
          'description' => 'ADD NEW USER'
        ],
        [
          'group' => 'USER ROLE',
          'name' => 'UPDATE USER',
          'description' => 'UPDATE USER'
        ],
        [
          'group' => 'USER ROLE',
          'name' => 'RESET PASSWORD USER',
          'description' => 'GENERATE NEW USER PASSWORD'
        ],
        [
          'group' => 'USER ROLE',
          'name' => 'ADD USER ACCESS',
          'description' => 'ADD USER ACCESS'
        ],
        [
          'group' => 'USER ROLE',
          'name' => 'DELETE USER ACCESS',
          'description' => 'DELETE USER ACCESS'
        ],
    ];

      foreach($roles as $role) {
        $accounting_role = Role::create([
          'group' => $role['group'],
          'name' => $role['name'],
          'description' => $role['description']
        ]);
      }
		}
}
