<?php

use App\UnitOfMeasure as Unit;
use Illuminate\Database\Seeder;

class UnitOfMeasuresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* 
         * Array Pattern 
         * @array = [ [ code, base_qty, base_unit, equal_qty, equal_unit ] ]
        */
        $units = [
            [
                "code" => "PCS",
                "base_qty" => 1,
                "base_unit" => "PCS",
                "equal_qty" => 1,
                "equal_unit" => "PCS"
            ],
            [
                "code" => "12 PCS/BOX",
                "base_qty" => 1,
                "base_unit" => "BOX",
                "equal_qty" => 12,
                "equal_unit" => "PCS"
            ],
            [
                "code" => "BAG",
                "base_qty" => 1,
                "base_unit" => "BAG",
                "equal_qty" => 1,
                "equal_unit" => "BAG"
            ],
            [
                "code" => "GAL",
                "base_qty" => 1,
                "base_unit" => "GAL",
                "equal_qty" => 1,
                "equal_unit" => "GAL"
            ],
            [
                "code" => "200 PCS/BOX",
                "base_qty" => 1,
                "base_unit" => "PCS",
                "equal_qty" => 1,
                "equal_unit" => "PCS"
            ],
            [
                "code" => "LTR",
                "base_qty" => 1,
                "base_unit" => "LTR",
                "equal_qty" => 1,
                "equal_unit" => "LTR"
            ],
            [
                "code" => "24 PCS/BOX",
                "base_qty" => 1,
                "base_unit" => "PCS",
                "equal_qty" => 1,
                "equal_unit" => "PCS"
            ],
            [
                "code" => "24 PCS/BOX",
                "base_qty" => 1,
                "base_unit" => "BOX",
                "equal_qty" => 24,
                "equal_unit" => "PCS"
            ],
            [
                "code" => "4 GAL/BOX",
                "base_qty" => 1,
                "base_unit" => "GAL",
                "equal_qty" => 1,
                "equal_unit" => "GAL"
            ],
            [
                "code" => "4 GAL/BOX",
                "base_qty" => 1,
                "base_unit" => "BOX",
                "equal_qty" => 4,
                "equal_unit" => "GAL"
            ],
            [
                "code" => "1000G/1KG",
                "base_qty" => 1,
                "base_unit" => "BAG",
                "equal_qty" => 1,
                "equal_unit" => "BAG"
            ],
            [
                "code" => "1000G/1KG",
                "base_qty" => 1,
                "base_unit" => "BOX",
                "equal_qty" => 1000,
                "equal_unit" => "BAG"
            ],
            [
                "code" => "100 PCS/BOX",
                "base_qty" => 1,
                "base_unit" => "PCS",
                "equal_qty" => 1,
                "equal_unit" => "PCS"
            ],
            [
                "code" => "100 PCS/BOX",
                "base_qty" => 1,
                "base_unit" => "BOX",
                "equal_qty" => 100,
                "equal_unit" => "PCS"
            ],
            [
                "code" => "LOT",
                "base_qty" => 1,
                "base_unit" => "LOT",
                "equal_qty" => 1,
                "equal_unit" => "LOT"
            ],
            [
                "code" => "UNIT",
                "base_qty" => 1,
                "base_unit" => "UNIT",
                "equal_qty" => 1,
                "equal_unit" => "UNIT"
            ],
            [
                "code" => "ROLL",
                "base_qty" => 1,
                "base_unit" => "ROLL",
                "equal_qty" => 1,
                "equal_unit" => "ROLL"
            ],
            [
                "code" => "300 PCS/BOX",
                "base_qty" => 1,
                "base_unit" => "PCS",
                "equal_qty" => 1,
                "equal_unit" => "PCS"
            ],
            [
                "code" => "300 PCS/BOX",
                "base_qty" => 1,
                "base_unit" => "BOX",
                "equal_qty" => 300,
                "equal_unit" => "PCS"
            ],
            [
                "code" => "500 PCS/BOX",
                "base_qty" => 1,
                "base_unit" => "BOX",
                "equal_qty" => 1,
                "equal_unit" => "BOX"
            ],
            [
                "code" => "500 PCS/BOX",
                "base_qty" => 1,
                "base_unit" => "PCS",
                "equal_qty" => 500,
                "equal_unit" => "BOX"
            ],
            [
                "code" => "MTR",
                "base_qty" => 1,
                "base_unit" => "MTR",
                "equal_qty" => 1,
                "equal_unit" => "MTR"
            ],
            [
                "code" => "CU",
                "base_qty" => 1,
                "base_unit" => "CU",
                "equal_qty" => 1,
                "equal_unit" => "CU"
            ],
            [
                "code" => "FT",
                "base_qty" => 1,
                "base_unit" => "FT",
                "equal_qty" => 1,
                "equal_unit" => "FT"
            ],
            [
                "code" => "KG",
                "base_qty" => 1,
                "base_unit" => "KG",
                "equal_qty" => 1,
                "equal_unit" => "KG"
            ],
            [
                "code" => "PAIL",
                "base_qty" => 1,
                "base_unit" => "PAIL",
                "equal_qty" => 1,
                "equal_unit" => "PAIL"
            ],
            [
                "code" => "TIN",
                "base_qty" => 1,
                "base_unit" => "TIN",
                "equal_qty" => 1,
                "equal_unit" => "TIN"
            ],
            [
                "code" => "SQM",
                "base_qty" => 1,
                "base_unit" => "BAG",
                "equal_qty" => 1,
                "equal_unit" => "BAG"
            ],
            [
                "code" => "CAN",
                "base_qty" => 1,
                "base_unit" => "PCS",
                "equal_qty" => 1,
                "equal_unit" => "PCS"
            ],
            [
                "code" => "BOX",
                "base_qty" => 1,
                "base_unit" => "BAG",
                "equal_qty" => 1,
                "equal_unit" => "BAG"
            ],
            [
                "code" => "BOX",
                "base_qty" => 1,
                "base_unit" => "BOX",
                "equal_qty" => 12,
                "equal_unit" => "BAG"
            ],
            [
                "code" => "PACK",
                "base_qty" => 1,
                "base_unit" => "PACK",
                "equal_qty" => 1,
                "equal_unit" => "PACK"
            ],
            [
                "code" => "SAM",
                "base_qty" => 1,
                "base_unit" => "BOX",
                "equal_qty" => 1,
                "equal_unit" => "BOX"
            ],
            [
                "code" => "SAM",
                "base_qty" => 1,
                "base_unit" => "PCS",
                "equal_qty" => 12,
                "equal_unit" => "BOX"
            ],
            [
                "code" => "SAM1",
                "base_qty" => 1,
                "base_unit" => "PCS",
                "equal_qty" => 1,
                "equal_unit" => "PCS"
            ],
            [
                "code" => "CU.M",
                "base_qty" => 1,
                "base_unit" => "CU.M",
                "equal_qty" => 1,
                "equal_unit" => "CU.M"
            ],
            [
                "code" => "KG.S",
                "base_qty" => 1,
                "base_unit" => "KG.S",
                "equal_qty" => 1,
                "equal_unit" => "KG.S"
            ],
            [
                "code" => "BD.FT",
                "base_qty" => 1,
                "base_unit" => "BD.FT",
                "equal_qty" => 1,
                "equal_unit" => "BD.FT"
            ],
            [
                "code" => "SQ.FT",
                "base_qty" => 1,
                "base_unit" => "SQ.FT",
                "equal_qty" => 1,
                "equal_unit" => "SQ.FT"
            ],
            [
                "code" => "L.M",
                "base_qty" => 1,
                "base_unit" => "L.M",
                "equal_qty" => 1,
                "equal_unit" => "L.M"
            ],
            [
                "code" => "SETS",
                "base_qty" => 1,
                "base_unit" => "SET",
                "equal_qty" => 1,
                "equal_unit" => "SET"
            ],
            [
                "code" => "ML",
                "base_qty" => 1,
                "base_unit" => "CAN",
                "equal_qty" => 60,
                "equal_unit" => "ML"
            ]
        ];

        Unit::insert( $units );
    }
}