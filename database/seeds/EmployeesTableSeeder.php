<?php

use App\HR\Employee;
use App\HR\EmployeePan;
use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employees = new Employee([
            'last_name' => 'ADMIN',
            'first_name' => 'SYSTEM',
            'middle_name' => 'S',
            'hired_date' => '2018-04-16',
            'birth_date' => '1992-01-01',
            'civil_status' => 'MARRIED',
            'address' => 'ST. SAMPLE, TEST CITY',
            'contact_num' => '0932-123456',
            'spouse_name' => 'SYSTEM ADMIN',
            'no_of_children' => '2',
            'no_of_dependent' => '0',
            'mother_name' => 'SYSTEM ADMIN',
            'sss_no' => '33-3333-333',
            'tin_no' => '407-235-256',
            'philhealth_no' => '324-2452-234',
            'hdmf_no' => '493-345-235',
            'hdmf_ee' => '100',
        ]);

        $employees->save();

        $pan = new EmployeePan([
            'nature' => 'REGULAR',
            'department' => 'IT',
            'job_title' => 'ADMIN',
            'superior' => 'RONNIE G. ISURINA',
            'pay_class' => 'MONTHLY',
            'effectivity' => '2018-04-01',
            'basic_salary' => 15000,
            'other_allow' => 0,
            'rice_allow' => 300,
            'laundry_allow' => 150,
            'clothing_allow' => 150,
            'medical_allow' => 150,
            'ob_allow' => 1200,
            'ecola' => 319,
            'remarks' => 'MERIT INCREASE DUE TO 2016  ANNUAL PERFORMANCE APPRAISAL, CONGRATULATIONS AND KEEP UP THE GOOD WORK. THE 2000 INCREASE SHALL BE WITHHELD AND NOT TO INCLUDE ON PAYMENT PAYOUT FOR SAVING PURPOSES AS PER NSC ADVISE',
            'is_saved' => true,
        ]);
    
        $employees->employee_pans()->save($pan);
    }
}
