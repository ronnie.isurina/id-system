<?php

use App\User;
use App\Role;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $acc_role = Role::find(1);
    $exec_role = Role::find(2);

    $exec_user = new App\User;
    $exec_user->name = 'System Admin';
    $exec_user->email = 'admin@gmail.com';
    $exec_user->password = bcrypt('admin');
    $exec_user->save();
    $exec_user->roles()->attach($exec_role);
  }
}
