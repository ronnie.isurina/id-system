<?php

use App\Brand;
use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = [
            [  'code' =>  'BOYSEN', 'description' => 'BOYSEN' ],
            [  'code' =>  'STANLEY', 'description' => 'STANLEY' ],
        ];

        Brand::insert( $brands );
    }
}
