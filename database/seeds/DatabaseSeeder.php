<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);

        $this->call(UnitOfMeasuresTableSeeder::class);
        $this->call(MaterialTypesTableSeeder::class);
        $this->call(BrandsTableSeeder::class);
        $this->call(MaterialsTableSeeder::class);

        $this->call(EmployeesTableSeeder::class);
    }
}
