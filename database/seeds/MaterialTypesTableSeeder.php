<?php

use App\MaterialType as Type;
use Illuminate\Database\Seeder;

class MaterialTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [  'code' =>  'LABOR', 'description' => 'LABOR' ],
            [  'code' =>  'BREAKER', 'description' => 'BREAKER' ],
            [  'code' =>  'BRUSH', 'description' => 'BRUSH' ],
            [  'code' =>  'PESTICIDES', 'description' => 'PESTICIDES' ],
            [  'code' =>  'CEMENT', 'description' => 'CEMENT' ],
            [  'code' =>  'CHALK', 'description' => 'CHALK' ],
            [  'code' =>  'CHB', 'description' => 'CHB' ],
            [  'code' =>  'CLAMP', 'description' => 'CLAMP' ],
            [  'code' =>  'CONSUMABLE', 'description' => 'CONSUMABLE' ],
            [  'code' =>  'CUTTING DISK', 'description' => 'CUTTING DISK' ],
            [  'code' =>  'DOOR', 'description' => 'DOOR' ],
            [  'code' =>  'DOOR LOCK', 'description' => 'DOOR LOCK' ],
            [  'code' =>  'DRILLBIT', 'description' => 'DRILLBIT' ],
            [  'code' =>  'FENCING', 'description' => 'FENCING' ],
            [  'code' =>  'FIBER', 'description' => 'FIBER' ],
            [  'code' =>  'FITTING', 'description' => 'FITTING' ],
            [  'code' =>  'FIXED ASSET', 'description' => 'FIXED ASSET' ],
            [  'code' =>  'FIXTURE', 'description' => 'FIXTURE' ],
            [  'code' =>  'FLAT BAR', 'description' => 'FLAT BAR' ],
            [  'code' =>  'GRAVEL', 'description' => 'GRAVEL' ],
            [  'code' =>  'GROUT', 'description' => 'GROUT' ],
            [  'code' =>  'HOSE', 'description' => 'HOSE' ],
            [  'code' =>  'LUMBER', 'description' => 'LUMBER' ],
            [  'code' =>  'METAL', 'description' => 'METAL' ],
            [  'code' =>  'NAIL', 'description' => 'NAIL' ],
            [  'code' =>  'PAINT', 'description' => 'PAINT' ],
            [  'code' =>  'PANEL BOARD', 'description' => 'PANEL BOARD' ],
            [  'code' =>  'PIPE', 'description' => 'PIPE' ],
            [  'code' =>  'PROJECT', 'description' => 'PROJECT' ],
            [  'code' =>  'ROOFING', 'description' => 'ROOFING' ],
            [  'code' =>  'SAND', 'description' => 'SAND' ],
            [  'code' =>  'SCREW', 'description' => 'SCREW' ],
            [  'code' =>  'SEALANT', 'description' => 'SEALANT' ],
            [  'code' =>  'SEALER', 'description' => 'SEALER' ],
            [  'code' =>  'SKIMCOAT', 'description' => 'SKIMCOAT' ],
            [  'code' =>  'SOLVENT', 'description' => 'SOLVENT' ],
            [  'code' =>  'STAIR NOSING', 'description' => 'STAIR NOSING' ],
            [  'code' =>  'STEEL', 'description' => 'STEEL' ],
            [  'code' =>  'TAPE', 'description' => 'TAPE' ],
            [  'code' =>  'TILE TRIM', 'description' => 'TILE TRIM' ],
            [  'code' =>  'TILES', 'description' => 'TILES' ],
            [  'code' =>  'TOOLS', 'description' => 'TOOLS' ],
            [  'code' =>  'WATERPROFING', 'description' => 'WATERPROFING' ],
            [  'code' =>  'WIRE', 'description' => 'WIRE' ],
            [  'code' =>  'WOOD', 'description' => 'WOOD' ],
            [  'code' =>  'PANEL', 'description' => 'PANEL' ],
            [  'code' =>  'CONCRETE', 'description' => 'CONCRETE' ],
            [  'code' =>  'REBARS', 'description' => 'REBARS' ],
            [  'code' =>  'OUTSOURCE', 'description' => 'OUTSOURCE' ],
            [  'code' =>  'STONE', 'description' => 'STONE' ],
            [  'code' =>  'BOARD', 'description' => 'BOARD' ],
            [  'code' =>  'SET', 'description' => 'SET' ],
            [  'code' =>  'OTHERS', 'description' => 'OTHERS' ],
            [  'code' =>  'UPVC', 'description' => 'UPVC DOORS AND WINDOWS' ],
            [  'code' =>  'GRANITE', 'description' => 'GRANITE' ],
            [  'code' =>  'KITCHEN', 'description' => 'KITCHEN' ],
            [  'code' =>  'ELECTRICAL', 'description' => 'ELECTRICAL' ],
            [  'code' =>  'CLADDING', 'description' => 'CLADDING' ],
            [  'code' =>  'G.I', 'description' => 'G.I' ],
            [  'code' =>  'NEMA', 'description' => 'NEMA' ],
            [  'code' =>  'LIGHT', 'description' => 'LIGHT' ],
            [  'code' =>  'PLANTS', 'description' => 'PLANTS' ],
            [  'code' =>  'CONDUIT', 'description' => 'CONDUIT' ],
            [  'code' =>  'SHAFTING', 'description' => 'SHAFTING' ],
            [  'code' =>  'HINGES', 'description' => 'HINGES' ],
            [  'code' =>  'LADDER', 'description' => 'LADDER' ],
            [  'code' =>  'EDGING', 'description' => 'EDGING' ],
            [  'code' =>  'ADHESIVE', 'description' => 'ADHESIVE' ]
        ];

        Type::insert( $types );
    }
}