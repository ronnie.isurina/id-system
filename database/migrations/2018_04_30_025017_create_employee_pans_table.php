<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeePansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_pans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->string('nature');
            $table->string('department');
            $table->string('job_title');
            $table->string('superior');
            $table->string('pay_class');
            $table->date('effectivity');
            $table->float('basic_salary', 16, 4);
            $table->float('other_allow', 16, 4)->default(0);
            $table->float('rice_allow', 16, 4)->default(0);
            $table->float('laundry_allow', 16, 4)->default(0);
            $table->float('clothing_allow', 16, 4)->default(0);
            $table->float('medical_allow', 16, 4)->default(0);
            $table->float('ob_allow', 16, 4)->default(0);
            $table->float('ecola', 16, 4)->default(0);
            $table->float('pay_13th', 16, 4)->default(0);
            $table->string('remarks', 2500)->nullable();
            $table->boolean('is_saved')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_pans');
    }
}
