<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('last_name');
            $table->string('first_name');
            $table->string('middle_name');
            $table->date('hired_date');
            $table->date('birth_date');
            $table->enum('civil_status', [
                'SINGLE', 
                'MARRIED',
                'SEPARATED',
                'WIDOW',
                'DIVORCED',
                'ANNULLED',
                'WIDOWER',
                'SINGLE PARENT'
            ]);
            $table->string('address')->nullable();
            $table->string('contact_num')->nullable();
            $table->string('spouse_name')->nullable();
            $table->integer('no_of_children')->nullable();
            $table->integer('no_of_dependent')->default(0);
            $table->string('mother_name')->nullable();
            $table->string('sss_no')->nullable();
            $table->string('tin_no')->nullable();
            $table->string('philhealth_no')->nullable();
            $table->string('hdmf_no')->nullable();
            $table->float('hdmf_ee')->default(100);
            $table->string('payroll_account')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
